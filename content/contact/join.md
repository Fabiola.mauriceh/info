---
id: 248
title: Jobs and Internships
date: 2010-03-31T18:30:31-04:00
author: n8fr8
layout: page
guid: https://guardianproject.info/?page_id=248
menu:
  main:
    name: Work or Intern With Us
    parent: contact
---
[<img title="teamguardianhardatwork" src="https://guardianproject.info/wp-content/uploads/2010/03/teamguardianhardatwork.jpg" alt="" width="576" height="384" />  
](https://guardianproject.info/wp-content/uploads/2010/03/teamguardianhardatwork.jpg) _Don&#8217;t let the visual anonymity fool you &#8211; Team Guardian is hard at work and proud of it!_

### Jobs

**You** &#8211; We&#8217;re looking for developers of all levels to join us &#8211; primarily mobile application developers with skills ranging from user interface design down to the core app work at the network and data level. For experienced developers we expect you to have at least one app released in public and prefer that you have experience with open-source and community-led projects. Experience with security software and standards is desired, but not required. The ability to assist in the localization of software to different regions / languages is a plus.

**Responsibilities** &#8211; Dependent on seniority, but ideally you&#8217;ll be able to confidently take the reins of the overall project engineering processes &#8211; specifically software build and release, version control management and software testing. Act as the lead engineer and be responsible for pushing releases to app store(s), markets and other distribution points. Be the team&#8217;s primary internal contributor of original project code.

All work is contract-based and is not limited by geography.

If you are interested in the opportunities below, send a resume or link to relevant work [through one of our contact methods](https://guardianproject.info/contact/).

### Internships

We&#8217;re always on the look-out for energetic, enthusiastic, and capable interns.

**You** &#8211; While an aptitude for development is preferred, all that is required is a passion for mobile devices, an interest in security, the eagerness to learn and the ability to be self-directed and productive.

**The Opportunity** &#8211; You will be exposed to some of the most advanced work being done in the field of mobile privacy, while at the same time contributing to the general good of humanity and gaining real-world production development experience. If that isn&#8217;t enough, then you may also end up with a Guardian-enabled Android device of your own.

**Responsibilities** &#8211; Your work will include testing software, working with NGOs and activist groups to setup and use Guardian software, producing online tutorials and guides, and even social media promotion work.

Interested? [Learn how to contact us here](https://guardianproject.info/contact/) or email &#x6a;&#111;b&#x73;&#x40;gu&#x61;&#114;d&#x69;&#x61;np&#x72;&#111;j&#x65;&#x63;&#116;.&#x69;&#x6e;f&#x6f; with an introduction and resume.

### CURRENT OPEN POSITIONS

No specific positions at the moment.

&nbsp;
